/*

Copyright (c) 2016 KAIST 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

//#include <ctime>
//#include <string>
//#include <iterator>
#include <iostream>
#include <fstream>
#include <cmath>
#include <limits>
#include <iomanip> 
#include "Faddeeva.hpp"

#include <mkl.h>
#include <mkl_blas.h>
#include <omp.h>

#include <cstring>
#include <sys/time.h>
#include <sys/mman.h>
#include <assert.h>


#define ALLOC alloc_if(1)
#define FREE free_if(1)
#define RETAIN free_if(0)
#define REUSE alloc_if(0)

//#define MIC_ID 0

#define N_THREADS 20
#define N_PHI     2
#define chunk_size 2

double compute_F(const double t, const double x_bar, const double scaling){
    double return_val;

    if (x_bar < 1.e-30){
        return_val= sqrt(scaling) * erf(M_PI / (2.0*scaling*t));
    }
    else{
        std::complex<double> z( M_PI/(2*scaling*t), t*x_bar);
        std::complex<double> w_iz = Faddeeva::erfcx(z);
        return_val=exp(-t*t*x_bar*x_bar);
        return_val-=(exp(-t*t*x_bar*x_bar-z*z)*w_iz).real();
        return_val*=sqrt(scaling);
    }
    return return_val;
}
int construct_F(const int axis, const int t_size, double* t_values,
        int* size, double* scaling, double** grid,
        double**& F_values  )
{
    for(int i_t=0; i_t<t_size; i_t++){
        F_values[i_t] = (double *) mkl_malloc (size[axis]*size[axis]*sizeof(double), 64);
        for(int i=0;i<size[axis];i++){
            for(int j=0; j<size[axis]; j++){
                F_values[i_t][i*size[axis]+j]=compute_F( t_values[i_t], fabs(grid[axis][i]-grid[axis][j]), scaling[axis]);
            }
        }
    }    
    return 0;
}
int sumvec(const int length, const double coef1, double* vector1,
           const double coef2, double* vector2, double*& vector3){
    #pragma omp parallel for 
    for(int i=0;i<length;i++){
        vector3[i] = coef1*vector1[i]+coef2*vector2[i];
    }
    return 0;
}

int sumvec(const int length, double* vector2, double*& vector3){
    #pragma omp parallel for schedule(runtime)
    for(int i=0;i<length;i++){
        vector3[i] += vector2[i];
    }
    return 0;
}

int gen_grid(int* size, double* scaling, double**& grid){

    for (int i =0;i<3;i++){
        double tmp_scaling = scaling[i];
        int tmp_size = size[i];
        for (int j =0;j<tmp_size;j++){
            grid[i][j]=-tmp_scaling*(0.5*(tmp_size-1)-j);
        }
    }
    return 0;
}
// Given the lower and upper limits of integration x1 and x2, and given n, this routine returns
// arrays x[1..n] and w[1..n] of length n, containing the abscissas and weights of the Gauss-
// Legendre n-point quadrature formula.
void gauleg(const double x1, const double x2, double* x, double* w, const int n){
    double EPS = 3.0E-11; // Relative precision
    int m, j, i;
    double z1, z, xm, xl, pp, p3, p2, p1; // High precision is a good idea for this routine.
    m = (n+1)/2;      // The roots are symmetric in the interval, so
    xm = 0.5*(x2+x1); // we only have to find half of them.
    xl = 0.5*(x2-x1);
    for(i=1;i<=m;i++){ // Loop over the desired roots.
        z = cos(3.141592654*(i-0.25)/(n+0.5));
        // Starting with the above approximation to the ith root, we enter the main loop of
        // refinement by Newton’s method.
        do{
            p1 = 1.0;
            p2 = 0.0;
            for(j=1;j<=n;j++){ // Loop up the recurrence relation to get the
                p3 = p2;       // Legendre polynomial evaluated at z.
                p2 = p1;
                p1 = ((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j;
            }
            // p1 is now the desired Legendre polynomial. We next compute pp, its derivative,
            // by a standard relation involving also p2, the polynomial of one lower order.
            pp = n*(z*p1-p2)/(z*z-1.0);
            z1 = z;
            z = z1-p1/pp; // Newton’s method.
        } while (fabs(z-z1) > EPS);
        x[i-1] = xm-xl*z;                  // Scale the root to the desired interval,
        x[n-i] = xm+xl*z;              // and put in its symmetric counterpart.
        w[i-1] = 2.0*xl/((1.0-z*z)*pp*pp); // Compute the weight
        w[n-i] = w[i-1];                 // and its symmetric counterpart.
    }
    return;
}

int t_sampling(const int num_points1, const int num_points2,
               const double t_i, const double t_l, const double t_f,
               double*& t_values, double*& w_t)
{
    // New method by Sundholm (JCP 132, 024102, 2010).
    // Within two-divided regions ([t_i,t_l], [t_l,t_f]),
    // num_points1, num_points2 quadrature points are made, respectively.

    // Linear coord region:  [t_i, t_l]

    double* x_leg = new double [num_points1];
    double* w_leg = new double [num_points1];
    gauleg(t_i, t_l, x_leg, w_leg, num_points1);

    for(int j=0; j<num_points1; j++){
        t_values[j] = x_leg[j];
        w_t[j] = w_leg[j]*2.0/sqrt(M_PI);
    }

    delete[] w_leg;
    delete[] x_leg;

    // Logarithmic coord region: [t_l, t_f]
    x_leg = new double [num_points2];
    w_leg = new double [num_points2];
    gauleg(log(t_l), log(t_f), x_leg, w_leg, num_points2);

    // Return the log-coord-partitioned points back to linear t-space.
    double s_p, w_p;
    for(int j=0; j<num_points2; j++){
        s_p = x_leg[j];
        w_p = w_leg[j];
        x_leg[j] = exp(s_p);
        w_leg[j] = w_p * exp(s_p);
    }

    for(int j=0; j<num_points2; j++){
        t_values[num_points1+j] = x_leg[j];
        w_t[num_points1+j] = w_leg[j]*2.0/sqrt(M_PI);
    }

    delete[] w_leg;
    delete[] x_leg;

    return 0;
}

int init_density(int num_gaussian, double** positions, double* coef, double* exponents,
                 int* size, double* scaling, double** grid, double*& density)
{
    /*
    double x,y,z,r; 
    double r2;
    */
    const double weight = scaling[0]*scaling[1]*scaling[2];
#pragma omp parallel for 
    for (int i_x=0; i_x <size[0]; i_x++){
        for (int i_y=0; i_y<size[1]; i_y++){
            for(int i_z=0; i_z<size[2]; i_z++){
                int index=i_x*size[1]*size[2]+i_y*size[2]+i_z;
                for(int i=0; i<num_gaussian; i++){
                    double x = grid[0][i_x]-positions[i][0];
                    double y = grid[1][i_y]-positions[i][1];
                    double z = grid[2][i_z]-positions[i][2];
                    double r2 = x*x+y*y+z*z;

                    double exponent2 = exponents[i]*exponents[i];
                    density[index] += coef[i]*pow(exponent2/M_PI,1.5)
                                       *exp(-1*exponent2*r2)*sqrt(weight);
                }
            }
        }
    }
    return 0;
}

__declspec(target(mic:phi_id))
__inline static
int offload_compute(const int* size,  double my_w_t, double *i_t_F_xs, double *i_t_F_ys, double *i_t_F_zs, double** d_rho, double** d_T_gamma, double** d_T_gamma2, double** d_T_beta, double** d_T_beta2, double** d_tmp_F_x, double** d_tmp_F_y, double** d_tmp_F_z) {
           const double one =1.0;
           const double zero=0.0;
           const CBLAS_TRANSPOSE transA [1]={CblasNoTrans};
           const CBLAS_TRANSPOSE transB [1]={CblasNoTrans};
           int size_per_group[1]={size[2]};

           #pragma omp parallel for
           #pragma vector aligned
           for(int i_z=0; i_z<size[2]; i_z++){
               d_tmp_F_x[i_z]=i_t_F_xs;
               d_tmp_F_y[i_z]=i_t_F_ys;
           }
           cblas_dgemm_batch(CblasColMajor,transA,transB,
                             &size[0], &size[1], &size[0], &one, (const double**) d_tmp_F_x, &size[0],
                             (const double**) d_rho, &size[1], &zero, d_T_gamma, &size[1], 1, size_per_group);
           cblas_dgemm_batch(CblasColMajor,transA,transB,
                             &size[0], &size[1], &size[1], &one, (const double**)  d_T_gamma, &size[0],
                             (const double**) d_tmp_F_y, &size[1], &zero, d_T_gamma2, &size[0], 1, size_per_group);

         #pragma omp parallel for
           for(int k=0; k<size[2]; k++){
               for(int j =0; j<size[1]; j++){
                  #pragma simd
                  for (int i=0; i<size[0]; i++){
                       d_T_beta[j][size[0]*k+i]=d_T_gamma2[k][size[0]*j+i];
                  }
               }
           }
           size_per_group[0]=size[1];
           for(int i_y=0; i_y<size[1]; i_y++){
               d_tmp_F_z[i_y]=i_t_F_zs;
           }
           cblas_dgemm_batch(CblasColMajor,transA,transB,
                             &size[0], &size[2], &size[2], &my_w_t, (const double**) d_T_beta, &size[0],
                             (const double**) d_tmp_F_z, &size[2], &one, d_T_beta2, &size[0], 1, size_per_group);
    return 0;
}

int compute_potential(int* size, const int t_size, double* t_values, double* w_t, 
                      double* density, double**  F_xs, double**   F_ys, double**  F_zs,
                      double*& potential)
{
    struct timeval start, end;
    double spent_time;

    const int total_size = size[0]*size[1]*size[2];
    double* rho [size[2]];
    double** T_gamma;
    double** T_gamma2;
    double** T_beta;
    double** T_beta2;

    double** d_rho;
    double** d_T_gamma;
    double** d_T_gamma2;
    double** d_T_beta;
    double** d_T_beta2;
    double** d_tmp_F_x ;
    double** d_tmp_F_y;
    double** d_tmp_F_z;
    __attribute__((target(mic))) double* d_potentials[N_PHI];

    /// init tag
    int phi_state=0;  // 0~3             kernel step;
    int cpu_state=0;  //
    int phi_count = 0;
    int cpu_count = 0;

    int phi_i_t =0;
    int cpu_i_t;
    int i_t=0;

    const int n_compute_threads=N_THREADS-1;
       
    for(int k=0; k<size[2]; k++){
        rho[k] = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
    }
    T_gamma = (double **)mkl_malloc(sizeof(double *)*size[2],64);
    T_gamma2 = (double **)mkl_malloc(sizeof(double *)*size[2],64);
    T_beta  = (double **)mkl_malloc(sizeof(double *)*size[1],64);
    T_beta2 = (double **)mkl_malloc(sizeof(double *)*size[1],64);
    for (int k=0;k<size[2];k++) {
        T_gamma[k] = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
        T_gamma2[k] = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
    }
    for(int j =0; j<size[1]; j++){
        T_beta[j] = (double *) mkl_malloc (size[0]*size[2]*sizeof(double), 64);
        T_beta2[j] = (double *) mkl_malloc (size[0]*size[2]*sizeof(double), 64);
    }
    for (int i=0;i<N_PHI;i++) { 
        d_potentials[i] = (double *) mkl_malloc (total_size*sizeof(double), 64);
    }

    double** tmp_F_x = (double **) mkl_malloc (sizeof(double *)* size[2], 64);
    double** tmp_F_y = (double **) mkl_malloc (sizeof(double *)* size[2], 64);
    double** tmp_F_z = (double **) mkl_malloc (sizeof(double *)* size[1], 64);
    omp_lock_t writelock;
    omp_init_lock(&writelock);

    int rank,local_first,local_end;
    int single_rank;
    int index;
    omp_set_nested(1);

    #pragma omp parallel private(rank,local_first,local_end,spent_time,start,end,phi_i_t,phi_count) num_threads(N_PHI+1)
    {
        rank = omp_get_thread_num();

        if (rank==N_PHI) {
            #pragma omp parallel for num_threads(N_THREADS-N_PHI)
            for(int i_z=0; i_z<size[2]; i_z++ ){
                rho[i_z]=(double *) mkl_malloc (sizeof(double *)*size[1]*size[0],64);
            }
            #pragma omp parallel for num_threads(N_THREADS-N_PHI)
            #pragma simd
            for(int i =0; i<total_size; i++){
                int x = i/(size[1]*size[2]);
                int y = (i-x*size[1]*size[2])/size[2];;
                int z = i%size[2];
                rho[z][x*size[1]+y]=density[x*size[1]*size[2]+y*size[2]+z];
            }
            mkl_set_num_threads_local(N_THREADS-N_PHI); 
            while(true) {

                gettimeofday(&start, NULL);
				if(cpu_count%chunk_size==0){
            	    omp_set_lock(&writelock);
        	        cpu_i_t = i_t;
    	            i_t+=chunk_size;
	                omp_unset_lock(&writelock);
				}
				else{
					cpu_i_t++;
				}
                if (cpu_i_t>=t_size ) break;

                cpu_count ++;
             

               // execute at xeon cpu
                const double one =1.0;
                const double zero=0.0;
    
                const CBLAS_TRANSPOSE transA [1]={CblasNoTrans };
                const CBLAS_TRANSPOSE transB [1]={CblasNoTrans };
                int size_per_group[1]={size[2]};
    
                #pragma omp parallel for num_threads(N_THREADS-N_PHI)
                #pragma vector aligned
                for(int i_z=0; i_z<size[2]; i_z++){
                    //tmp_F_x[i_z]=F_xs[cpu_i_t[i_group]];
                    //tmp_F_y[i_z]=F_ys[cpu_i_t[i_group]];
                    tmp_F_x[i_z]=F_xs[cpu_i_t];
                    tmp_F_y[i_z]=F_ys[cpu_i_t];
                }
    
                cblas_dgemm_batch(CblasColMajor,transA,transB,
                              &size[0], &size[1], &size[0], &one, (const double**) tmp_F_x, &size[0],
                              //(const double**) rho, &size[1], &zero, T_gammas[i_group], &size[1], 1, size_per_group);
                              (const double**) rho, &size[1], &zero, T_gamma, &size[1], 1, size_per_group);
                cblas_dgemm_batch(CblasColMajor,transA,transB,
                              &size[0], &size[1], &size[1], &one, (const double**)  T_gamma, &size[0],
                              (const double**) tmp_F_y, &size[1], &zero, T_gamma2, &size[0], 1, size_per_group);
                              //&size[0], &size[1], &size[1], &one, (const double**)  T_gammas[i_group], &size[0],
                              //(const double**) tmp_F_y, &size[1], &zero, T_gamma2s[i_group], &size[0], 1, size_per_group);
                #pragma omp parallel for num_threads(N_THREADS-N_PHI)
                for(int k=0; k<size[2]; k++){
                    for(int j =0; j<size[1]; j++){
                       //#pragma vector aligned
                       #pragma simd
                       for (int i=0; i<size[0]; i++){
                           //T_betas[i_group][j][size[0]*k+i]=T_gamma2s[i_group][k][size[0]*j+i];
                           T_beta[j][size[0]*k+i]=T_gamma2[k][size[0]*j+i];
                       }
                     }
                }
                size_per_group[0]=size[1];
                for(int i_y=0; i_y<size[1]; i_y++){
                   //tmp_F_z[i_y]=F_zs[cpu_i_t[i_group]];
                   tmp_F_z[i_y]=F_zs[cpu_i_t];
                }
                cblas_dgemm_batch(CblasColMajor,transA,transB,
                              &size[0], &size[2], &size[2], &w_t[cpu_i_t], (const double**) T_beta, &size[0],
                              (const double**) tmp_F_z, &size[2], &one, T_beta2, &size[0], 1, size_per_group);
                             //&size[0], &size[2], &size[2], &one, (const double**) T_betas[i_group], &size[0],
                             //(const double**) tmp_F_z, &size[2], &zero, T_beta2s[i_group], &size[0], 1, size_per_group);
   
               gettimeofday(&end, NULL);
               spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
               std::cout <<"cpu time\t"<< cpu_i_t <<"\t" << omp_get_thread_num() << "\t" << spent_time <<std::endl;
            }
            //gettimeofday(&end, NULL);
            //clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */
            //std::cout << "portion of locking:\t"<<((double) locking_time) /(((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) ) <<std::endl;
        } else {

        //#pragma omp master
            int phi_id = omp_get_thread_num();
             
            fprintf(stderr, "thread id %d makes offloading\n", phi_id);  
            gettimeofday(&start, NULL);
                // nocopy(d_T[phi_id]:length(total_size) ALLOC RETAIN)

            #pragma offload target(mic:phi_id) \
                 in(size:length(3) ALLOC RETAIN) \
                 in(density:length(total_size) align(64) ALLOC RETAIN) \
                 nocopy(d_rho, d_tmp_F_x, d_tmp_F_y, d_tmp_F_z) \
                 nocopy(d_T_gamma, d_T_gamma2, d_T_beta, d_T_beta2)\ 
                 nocopy(d_potentials[phi_id]:length(total_size) ALLOC RETAIN)                                                         
            {
                 d_rho = (double **) mkl_malloc( sizeof(double *)*(size[2]),64);

                 #pragma omp parallel for 
                 for(int i_z=0; i_z<size[2]; i_z++ ){
                     d_rho[i_z]=(double *) mkl_malloc (sizeof(double *)*size[1]*size[0],64);
                 }
                 #pragma omp parallel for 
                 #pragma simd
                 for(int i =0; i<total_size; i++){
                     int x = i/(size[1]*size[2]);
                     int y = (i-x*size[1]*size[2])/size[2];;
                     int z = i%size[2];
                     d_rho[z][x*size[1]+y]=density[x*size[1]*size[2]+y*size[2]+z];
                 }
                 d_tmp_F_x = (double **) mkl_malloc (sizeof(double *)*size[2], 64);
                 d_tmp_F_y = (double **) mkl_malloc (sizeof(double *)*size[2], 64);
                 d_tmp_F_z = (double **) mkl_malloc (sizeof(double *)*size[1], 64);
                 d_T_gamma = (double **) mkl_malloc (sizeof(double *)*size[2], 64);
                 d_T_gamma2 = (double **) mkl_malloc (sizeof(double *)*size[2], 64);
                 d_T_beta= (double **) mkl_malloc (sizeof(double *)*size[1], 64);
                 d_T_beta2 = (double **) mkl_malloc (sizeof(double *)*size[1], 64);
     	        #pragma omp parallel for 
     	        #pragma vector aligned
                 for(int i_z=0; i_z< size[2]; i_z++){
     	            d_T_gamma[i_z]  = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
     		        d_T_gamma2[i_z] = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
     	         }
     
     	        #pragma omp parallel for 
     	        #pragma vector aligned
     	        for(int i_y =0; i_y < size[1]; i_y++ ){
                     d_T_beta[i_y] = (double *) mkl_malloc (size[0]*size[2]*sizeof(double), 64);
                     d_T_beta2[i_y] = (double *) mkl_malloc (size[0]*size[2]*sizeof(double), 64);
     	        }
            }
	        
    	    gettimeofday(&end, NULL);
    	    spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
    	    std::cerr <<"phi memory alloc" << phi_id << "#: " << spent_time << std::endl;

            while(true) {
                gettimeofday(&start, NULL);
				if(phi_count%chunk_size==0){
            	    omp_set_lock(&writelock);
        	        phi_i_t = i_t;
    	            i_t+=chunk_size;
	                omp_unset_lock(&writelock);
                //fprintf(stderr, "%d phi \n", phi_i_t);
                }
				else{
					phi_i_t++;
				}

				if(phi_i_t>=t_size) break;

                phi_count++;
			
                // execute at phi
                double *i_t_F_xs = F_xs[phi_i_t];
                double *i_t_F_ys = F_ys[phi_i_t];
                double *i_t_F_zs = F_zs[phi_i_t];
                const double my_w_t = w_t[phi_i_t];

                #pragma offload target(mic:phi_id) \
                       in(i_t_F_xs:length(size[0]*size[0]) align(64)) \
                       in(i_t_F_ys:length(size[1]*size[1]) align(64)) \
                       in(i_t_F_zs:length(size[2]*size[2]) align(64)) \
                       in(my_w_t) \
                       in(size:length(3) REUSE RETAIN) \
                       nocopy(d_rho, d_tmp_F_x, d_tmp_F_y, d_tmp_F_z) \
                       nocopy(d_T_gamma, d_T_gamma2, d_T_beta, d_T_beta2) 
                {
                    offload_compute(size, my_w_t, i_t_F_xs, i_t_F_ys, i_t_F_zs, d_rho, d_T_gamma, d_T_gamma2, d_T_beta, d_T_beta2, d_tmp_F_x, d_tmp_F_y, d_tmp_F_z);
                }

                gettimeofday(&end, NULL);
                spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
                std::cout <<"phi time #"<< phi_id<< "\t"<< phi_i_t <<"\t" << spent_time <<std::endl;
            }
            #pragma offload target(mic:phi_id) \
                   in(size:length(3) REUSE RETAIN) \
                   nocopy(d_T_beta2, d_potentials)  
            {
                #pragma omp parallel for
                #pragma vector aligned
                for(int i=0; i < total_size; i++){
                    int x = i/(size[1]*size[2]);
                    int y = (i-x*size[1]*size[2])/size[2];;
                    int z = i%size[2];
                    d_potentials[phi_id][x*size[1]*size[2]+y*size[2]+z]=d_T_beta2[y][size[0]*z+x];
                } 
            }
            #pragma offload target(mic:phi_id) \ 
                   in(size:length(3) REUSE FREE) \
                   out(d_potentials[phi_id]:length(total_size) FREE) \
                   nocopy(d_rho, d_tmp_F_x, d_tmp_F_y, d_tmp_F_z) \
                   nocopy(d_T_gamma, d_T_gamma2, d_T_beta, d_T_beta2)
            {
                mkl_free(d_tmp_F_x);
                mkl_free(d_tmp_F_y);
                mkl_free(d_tmp_F_z);
                mkl_free(d_T_gamma);
                mkl_free(d_T_gamma2);
                mkl_free(d_T_beta);
                mkl_free(d_T_beta2);
                for(int i_z=0; i_z< size[2]; i_z++){
                    mkl_free(d_T_gamma[i_z]);
                    mkl_free(d_T_gamma2[i_z]);
                }
            
                for(int i_y =0; i_y < size[1]; i_y++ ){
                    mkl_free(d_T_beta[i_y]);
                    mkl_free(d_T_beta2[i_y]);
                }
            }
            



        }
     }
     //std::cerr << "end parallel region"<< std::endl;
    #pragma omp parallel for 
    for(int i_y=0; i_y<size[1]; i_y++){
       for(int i =0; i <size[0]; i++){
          #pragma vector aligned
          for(int k = 0; k < size[2]; k++){
             potential[i*size[1]*size[2]+i_y*size[2]+k] = T_beta2[i_y][k*size[0]+i];
          }
       }
    }

    for(int n=0;n<N_PHI;n++) {
       #pragma omp parallel for 
       for(int i_y=0; i_y<size[1]; i_y++){
          for(int i =0; i <size[0]; i++){
             #pragma vector aligned
             for(int k = 0; k < size[2]; k++){
                 potential[i*size[1]*size[2]+i_y*size[2]+k] += d_potentials[n][i_y*size[2]*size[0]+size[0]*k+i];
             }
          }
       }
    }

    mkl_free( tmp_F_x );                                           
    mkl_free( tmp_F_y );                                           
    mkl_free( tmp_F_z );                                           

    for(int k=0; k<size[2]; k++){
       mkl_free( rho[k] );
    }
    mkl_free ( T_gamma );
    mkl_free ( T_gamma2 );
    mkl_free (T_beta );
    mkl_free( T_beta2 );
    for (int k=0;k<size[2];k++) {
        mkl_free ( T_gamma[k] );
        mkl_free ( T_gamma2[k] );
    }
    for(int j =0; j<size[1]; j++){
        mkl_free ( T_beta[j] );
        mkl_free ( T_beta2[j] );
    }
    for (int i=0;i<N_PHI;i++) {
        mkl_free (d_potentials[i]);
    }
    return 0;
}

double compute_energy(const int total_size, double* scaling, double* potential, double* density){
    double return_val=0.0;
    for(int i =0; i < total_size; i++){
        return_val += density[i]*potential[i];
    }
    return 0.5*return_val*sqrt(scaling[0]*scaling[1]*scaling[2]);
}
int read_input(std::string filename, int*& size, double*& scaling,
                int& num_gaussian, double**& positions, double*& coef, double*& exponents,
                int& num_points1, int& num_points2, double& t_i, double& t_l, double& t_f)
{
    FILE *file;
    file = fopen( filename.c_str(), "r" );
    if( file == NULL )
    {
        perror( "fopen" );
        return -1;
    }

    fscanf( file, "%d %d %d \n", &size[0], &size[1], &size[2] );
    fscanf( file, "%lf %lf %lf\n", &scaling[0], &scaling[1], &scaling[2] );
    fscanf( file, "%d %d \n", &num_points1, &num_points2 );
    fscanf( file, "%lf %lf %lf\n", &t_i, &t_l, &t_f);
    fscanf( file, "%d \n", &num_gaussian);

    positions = new double*[num_gaussian];
    coef = new double[num_gaussian];
    exponents = new double[num_gaussian];

    int linenum = 4;
    int count_gaussian = 0;
    while( count_gaussian<num_gaussian)
    {
        linenum++;

        positions[count_gaussian] = new double[3];
        if( fscanf( file, "%lf %lf %lf \n", &positions[count_gaussian][0], &positions[count_gaussian][1], &positions[count_gaussian][2] ) != 3 ) {
//            fprintf( stderr, "Error in file on line %i\n", linenum );
        }
        linenum++;
        if( fscanf( file, "%lf %lf \n", &coef[count_gaussian], &exponents[count_gaussian])) {
//            fprintf( stderr, "Error in file on line %i\n", linenum );
        }
        count_gaussian++;
    }
    return 0;
}

int main(int argc, char* argv[]){

    if(argc != 2){
        std::cout<< "Wrong input argument. \n";
        std::cout<< "You have to write 'executable inputfile' \n" ;
        std::cout<< "ex) ./exe input.txt \n" ;
        exit(EXIT_FAILURE);
    }

    std::string filename=argv[1];

    double* scaling = new double[3];
    int* size = new int[3];
    int num_points1; int num_points2;
    double t_i; double t_l; double t_f;

    int num_gaussian;
    double** positions;
    double* coef; double* exponents;
    read_input(filename, size, scaling, num_gaussian, positions, coef, exponents, num_points1, num_points2, t_i, t_l, t_f);

    const int t_size = num_points1+num_points2;

    int omp_num_threads=0;
    int thread_index=0;

    char processor_name[100];        

    
    #pragma omp parallel default(shared) private(thread_index, omp_num_threads)
    {
        omp_num_threads = omp_get_num_threads();
        thread_index = omp_get_thread_num();
    }
    
    const int total_size = size[0]*size[1]*size[2];
    
    std::cout << "======== input information ========" <<std::endl;
    std::cout << "size: " << size[0] <<"," << size[1]<<"," <<size[2] << std::endl;
    std::cout << "total_size: " <<  total_size <<std::endl;
    std::cout << "scaling: " <<scaling[0] << "," << scaling[1] <<"," << scaling[2] <<std::endl;
    std::cout << "num_points1: " << num_points1 << std::endl << "num_points2: " << num_points2 << std::endl;
    std::cout << "num_gaussian: " << num_gaussian << std::endl;
    std::cout << "===================================" <<std::endl;

    for(int q=0;q<num_gaussian;q++){
        std::cout << q << "th gaussian center:" << positions[q][0] <<","<< positions[q][1] <<","<< positions[q][2]  <<std::endl;
        std::cout << q << "th coef and exponenet:" << coef[q] <<","<< exponents[q]   <<std::endl;
    }

    double* t_values = (double *)mkl_malloc(sizeof(double)*t_size,64);
    double* w_t = (double *)mkl_malloc (sizeof(double)*t_size, 64);

    t_sampling(num_points1, num_points2, t_i, t_l, t_f, t_values, w_t );

    double** grid = (double **) mkl_malloc(sizeof(double*)*3, 64);
    grid[0] = (double *)mkl_malloc(sizeof(double)*size[0], 64);
    grid[1] = (double *)mkl_malloc(sizeof(double)*size[1], 64);
    grid[2] = (double *)mkl_malloc(sizeof(double)*size[2], 64);

    gen_grid(size,scaling,grid);

    double** F_xs = (double **)mkl_malloc(sizeof(double*)*t_size, 64);
    double** F_ys = (double **)mkl_malloc(sizeof(double*)*t_size, 64);
    double** F_zs = (double **)mkl_malloc(sizeof(double*)*t_size, 64);
    double* density = (double *)mkl_malloc(total_size*sizeof(double), 64);
//    #pragma offload target(mic:MIC_ID) \
//         in(density:length(total_size) align(64))
//    {
    #pragma omp parallel for 
    for(int i=0; i<total_size; i++){
        density[i]=0.0;
    }
//    }

    double* potential = (double *)mkl_malloc(total_size*sizeof(double), 64);
    
    time_t st = std::clock();
    init_density(num_gaussian, positions, coef, exponents, size, scaling, grid, density);
    std::cout << "initialize density: " << double(clock()- st)/CLOCKS_PER_SEC << "s"<<std::endl;  

    st = std::clock();
    construct_F(0, t_size, t_values, size, scaling, grid, F_xs);
    construct_F(1, t_size, t_values, size, scaling, grid, F_ys);
    construct_F(2, t_size, t_values, size, scaling, grid, F_zs);
    std::cout << "construct F matrices: " << double(clock()- st)/CLOCKS_PER_SEC << "s"<<std::endl;

    struct timeval start, end;
    gettimeofday(&start, NULL);
    compute_potential(size, t_size, t_values, w_t, density, F_xs, F_ys, F_zs, potential);
    sumvec(total_size, M_PI/(t_f*t_f),density,1.0,potential,potential);
    gettimeofday(&end, NULL);
    double spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
    std::cout << "! compute_potential: " << spent_time << "s"<<std::endl;

    double energy = compute_energy(total_size, scaling, density, potential);
    std::cout << std::setprecision(std::numeric_limits<double>::digits) << "energy:  " <<energy <<std::endl;

    mkl_free(grid[0]);
    mkl_free(grid[1]);
    mkl_free(grid[2]);
    mkl_free(grid);
    mkl_free( density );
    mkl_free( potential );
    for(int i_t=0;i_t<t_size;i_t++){
        mkl_free( F_xs[i_t] );
        mkl_free( F_ys[i_t] );
        mkl_free( F_zs[i_t] );
    }
    mkl_free( F_xs );
    mkl_free( F_ys );
    mkl_free( F_zs );
    mkl_free(w_t);
    return 0;
}


