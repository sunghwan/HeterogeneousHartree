/*

Copyright (c) 2016 KAIST 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include "Faddeeva.hpp"
#include <ctime>
#include <cmath>
#include <complex>
#include <omp.h>
#include "cublas_v2.h"
#include <mkl.h>
#include <mkl_blas.h>
#include <string.h>
#include <iostream>
#include <sys/time.h>
#include <pthread.h>
#include <fstream>
#include <stdio.h>
#include <iomanip>

#include <stdint.h>
#include <time.h>

#define N_GPU 1
#define N_THREADS 20
#define chunk_size 4

//const int max_problem_size=512;
//const int max_t_size=64;
const int num_threads = 1024;
const int num_blocks = 512;
///////////////////////////////////////////////////////////////

__constant__ int d_size[3];


bool checkRun(cudaStream_t* streams, int N){
    for(int i =N-1; i>=0; i-- ){
        if(cudaStreamQuery(streams[i])==cudaErrorNotReady  ){
            return true;
        }
    }
    return false;
}


double compute_F(const double t, const double x_bar, const double scaling){
    double return_val;

    if (x_bar < 1.e-30){
        return_val= sqrt(scaling) * erf(M_PI / (2.0*scaling*t));
    }
    else{
        std::complex<double> z( M_PI/(2*scaling*t), t*x_bar);
        std::complex<double> w_iz = Faddeeva::erfcx(z);
        return_val=exp(-t*t*x_bar*x_bar);
        return_val-=(exp(-t*t*x_bar*x_bar-z*z)*w_iz).real();
        return_val*=sqrt(scaling);
    }
    return return_val;
}
int construct_F(const int axis, const int t_size, double* t_values,
        int* size, double* scaling, double** grid,
        double**& F_values  )
{
    for(int i_t=0; i_t<t_size; i_t++){
        F_values[i_t] = (double *) mkl_malloc (size[axis]*size[axis]*sizeof(double), 64);
        for(int i=0;i<size[axis];i++){
            for(int j=0; j<size[axis]; j++){
                F_values[i_t][i*size[axis]+j]=compute_F( t_values[i_t], fabs(grid[axis][i]-grid[axis][j]), scaling[axis]);
            }
        }
    }

    return 0;
}
__global__ void transpose(double** original_matrices, double** matrices ){
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = d_size[0]*d_size[1]*d_size[2];
    const int stride = blockDim.x*gridDim.x;
    int x,y,z;

    for(int i=idx; i<total_size ; i+=stride){
        z = i/(d_size[1]*d_size[0]);
        y = (i-z*d_size[1]*d_size[0])/d_size[0];
        x = i%d_size[0];
    
        matrices[y][d_size[0]*z+x] = original_matrices[z][d_size[0]*y+x];
    }

    return;
}

__global__ void unfold(double const** __restrict__ original_matrices, double* values ){
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = d_size[0]*d_size[1]*d_size[2];
    const int stride = blockDim.x*gridDim.x;

    int x,y,z;
    for(int i=idx; i<total_size ; i+=stride){

        x = i/(d_size[1]*d_size[2]);
        y = (i-x*d_size[1]*d_size[2])/d_size[2];
        z = i%d_size[2];

        values[x*d_size[1]*d_size[2]+y*d_size[2]+z] = original_matrices[y][d_size[0]*z+x];
    }

    return;
}

__global__ void fold(double* values, double ** out){

    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = d_size[0]*d_size[1]*d_size[2];
    const int stride = blockDim.x*gridDim.x;

    int x,y,z;
    for(int i=idx; i<total_size ; i+=stride){

        x = i/(d_size[1]*d_size[2]);
        //y = i/d_size[2]-x*d_size[1];
        y = (i-x*d_size[1]*d_size[2])/d_size[2];
        z = i%d_size[2];
        out[y][d_size[0]*z+x]=values[x*d_size[1]*d_size[2]+y*d_size[2]+z] ;
    }
    return;
}

__global__ void gpu_sumvec(const int length,
    const double coef2, double const* __restrict__ vector2, double* __restrict__ vector3){
    const int start = blockDim.x*blockIdx.x + threadIdx.x;

    for (int i = start; i < length; i += num_blocks * num_threads){
        vector3[i] += coef2 * vector2[i];
    }
}

__host__ int sumvec(const int length, double* vector2, double*& vector3){
    #pragma omp parallel for schedule(runtime) 
    for(int i=0;i<length;i++){
        vector3[i] += vector2[i];
    }
    return 0;
}
__host__ int sumvec(const int length, 
        const double coef2, double* vector2, double*& vector3){
    #pragma omp parallel for schedule(runtime) 
    for(int i=0;i<length;i++){
        vector3[i] += coef2*vector2[i];
    }
    return 0;
}

int compute_potential(int* size, const int t_size, double* t_values, double* w_t,
        double* density, double** F_xs, double** F_ys, double** F_zs,
        double*& potential, double t_f)
{


    //cudaError_t error;
    //struct timeval start, end;
    struct timeval start, end;
    double spent_time;

    const int total_size = size[0] * size[1] * size[2];
    double one = 1.0; double zero = 0.0;
    ////////////////////// INIT TAGS ////////////////////////////////
    int* gpu_states = new int[N_GPU];
    int* gpu_count = new int [N_GPU]();
    int cpu_count = 0;

    int gpu_i_t =0;
    int cpu_i_t=0;
    int i_t=0;

    /////////////////////// CPU Memory Alloc //////////////////////////////////////
    double** rho = (double**) mkl_malloc(size[2]*sizeof(double*),64);
    for(int k=0; k<size[2]; k++){
        rho[k] = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
    }
    double* T = (double *) mkl_malloc (total_size*sizeof(double), 64);
    /////////////////////// Fold Density ////////////////////////////////////////

    /////////////////////// pthread Setting  //////////////////////////////////////
    omp_lock_t writelock;
    omp_init_lock(&writelock);


    ///////////////////////////// GPU SETTING //////////////////////////////////////////
    double** dFx_dev; double** dFy_dev; double** dFz_dev;                

    double* d_density;
    double** d_potential = new double*[N_GPU];

    /////////////////////// RUN  /////////////////////////////////

    const int stream_size = std::max(size[2],size[1]);
    cudaStream_t streamArray[stream_size*N_GPU];
    for(int i=0; i<stream_size*N_GPU; i++){
        cudaStreamCreate(&streamArray[i]);
    }
    int rank;
    omp_set_nested(1);
    double** gpu_potential = new double*[N_GPU];

    double** test_vector = new double*[t_size+1];// shchoi
    #pragma omp parallel firstprivate(spent_time,start,end,d_density,gpu_i_t) num_threads(N_GPU+1)
    {
        
        rank = omp_get_thread_num();
        if(rank==N_GPU)
        {
            #pragma omp parallel for schedule(runtime) num_threads(N_THREADS-N_GPU) 
            for(int i_z=0; i_z<size[2]; i_z++ ){
                #pragma ivdep
                for(int j=0; j<size[1]; j++){
                   for(int i=0; i<size[0]; i++){
                       rho[i_z][j*size[0]+i]=density[i*size[1]*size[2]+j*size[2]+i_z];
                   }
                }
            }
    
            double** T_gamma = (double**) mkl_malloc(size[2]*sizeof(double*),64);
            double** T_gamma2 = (double**) mkl_malloc(size[2]*sizeof(double*),64);
            double** T_beta = (double**) mkl_malloc(size[1]*sizeof(double*),64);
            double** T_beta2 = (double**) mkl_malloc(size[1]*sizeof(double*),64);
        
            for(int k=0; k<size[2]; k++){
                T_gamma[k] = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
                T_gamma2[k] = (double *) mkl_malloc (size[0]*size[1]*sizeof(double), 64);
            }
            for(int j =0; j<size[1]; j++){
                T_beta[j] = (double *) mkl_malloc (size[0]*size[2]*sizeof(double), 64);
                T_beta2[j] = (double *) mkl_malloc (size[0]*size[2]*sizeof(double), 64);
            }
        
            double** tmp_F_x= (double**) mkl_malloc(size[2]*sizeof(double*),64);
            double** tmp_F_y= (double**) mkl_malloc(size[2]*sizeof(double*),64);
            double** tmp_F_z= (double**) mkl_malloc(size[1]*sizeof(double*),64);

            struct timespec start_t, end_t;
            while(true){
                if(cpu_count%chunk_size==0){
                    omp_set_lock(&writelock);
                    cpu_i_t = i_t;
                    i_t++;
                    omp_unset_lock(&writelock);
                }
                else{
                    cpu_i_t++;
                }
                if(cpu_i_t>=t_size ) break;
                cpu_count++;
     
                //std::cout <<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<< cpu_i_t << "\tcpu\t" <<rank << "\t" << omp_get_thread_num()<<std::endl;
                mkl_set_num_threads_local(N_THREADS-N_GPU); 
                double one =1.0; 
                double zero=0.0;
     
                CBLAS_TRANSPOSE transA [1]={CblasNoTrans };
                CBLAS_TRANSPOSE transB [1]={CblasNoTrans };
                int size_per_group[1]={size[2]};
                for(int i_z=0; i_z<size[2]; i_z++){
                    //tmp_F_x[i_z]=F_xs[cpu_i_t[i_group]];
                    tmp_F_x[i_z]=F_xs[cpu_i_t];
                    tmp_F_y[i_z]=F_ys[cpu_i_t];
                    //tmp_F_y[i_z]=F_ys[cpu_i_t[i_group]];
                }
//                std::cout << cpu_i_t[i_group] << "\t cpu(" <<i_group<<")"<<std::endl;
                std::cout << cpu_i_t << "\t cpu" <<std::endl;
     
                cblas_dgemm_batch(CblasColMajor,transA,transB,
                                  &size[0], &size[1], &size[0], &one, (const double**) tmp_F_x, &size[0],
                                  (const double**) rho, &size[1], &zero, T_gamma, &size[1], 1, size_per_group);
                                  //(const double**) rho, &size[1], &zero, T_gammas[i_group], &size[1], 1, size_per_group);
                cblas_dgemm_batch(CblasColMajor,transA,transB,
                                  //&size[0], &size[1], &size[1], &one, (const double**)  T_gammas[i_group], &size[0],
                                  &size[0], &size[1], &size[1], &one, (const double**)  T_gamma, &size[0],
                                  (const double**) tmp_F_y, &size[1], &zero, T_gamma2, &size[0], 1, size_per_group);
                                  //(const double**) tmp_F_y, &size[1], &zero, T_gamma2s[i_group], &size[0], 1, size_per_group);
                #pragma omp parallel for schedule(runtime) num_threads(N_THREADS-N_GPU)
                for(int j =0; j<size[1]; j++){
                    for (int i=0; i<size[0]; i++){
                        for(int k=0; k<size[2]; k++){
                            T_beta[j][size[0]*k+i]=T_gamma2[k][size[0]*j+i];
                        }
                    }
                }
                size_per_group[0]=size[1];
                for(int i_y=0; i_y<size[1]; i_y++){
                    tmp_F_z[i_y]=F_zs[cpu_i_t];
                }
                cblas_dgemm_batch(CblasColMajor,transA,transB,
                                  &size[0], &size[2], &size[2], &w_t[cpu_i_t], (const double**) T_beta, &size[0],
                                  (const double**) tmp_F_z, &size[2], &one, T_beta2, &size[0], 1, size_per_group);
                //gettimeofday(&end, NULL);
                //spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
                //std::cout <<"cpu time"<< cpu_i_t <<"\t" << spent_time <<std::endl;
                //std::cout <<"cpu time"<< cpu_i_t[i_group] <<"\t" << omp_get_thread_num() << "\t" << spent_time <<std::endl;
            }
            #pragma omp parallel for schedule(runtime) num_threads(N_THREADS-N_GPU) 
            for(int i_y=0; i_y<size[2]; i_y++){
                for(int i =0; i <size[0]; i++){
                    for(int k = 0; k < size[2]; k++){
                        T[i*size[1]*size[2]+i_y*size[2]+k] = T_beta2[i_y][size[0]*k+i];
                    }
                }
            }
            mkl_free( tmp_F_x);
            mkl_free( tmp_F_y);
            mkl_free( tmp_F_z);

            for(int k=0; k<size[2]; k++){
                //delete[] rho[k];
                mkl_free(T_gamma[k]);
                mkl_free(T_gamma2[k]);
            }
            for(int j=0; j<size[1]; j++){
                mkl_free(T_beta[j]);
                mkl_free(T_beta2[j]);
            }
            mkl_free(T_gamma);
            mkl_free(T_gamma2);
            mkl_free(T_beta);
            mkl_free(T_beta2);
        }
        else{
            int gpu_id = omp_get_thread_num();
            if(cudaSuccess!=cudaSetDevice (gpu_id)){
                std::cerr << "cudaSetDevice Erorr" <<std::endl;
                std::cerr << cudaGetErrorString(cudaGetLastError()) <<std::endl;
            }
            cublasHandle_t handle;
            cublasCreate(&handle);

            cudaStream_t streamArray[stream_size];
            for(int i=0; i<stream_size; i++){
                cudaStreamCreateWithFlags(&streamArray[i],cudaStreamNonBlocking);
            }

            if (cudaMemcpyToSymbol(d_size, size, 3 * sizeof(int)) != cudaSuccess)
            {
                std::cout << "Error:: cudaMemcpyToSymbol (0) " << std::endl;
                printf("Error:shchoi:  %s\n", cudaGetErrorString(cudaGetLastError()));
            }
    
            std::cout << "N_GPU: "<<N_GPU << std::endl;
            std::cout << "gpu_id "<<gpu_id <<std::endl;
            gettimeofday(&start, NULL);

            double** drho;  double** drho_dev;
            double** dFx; double** dFy; double** dFz;                
            double** dT_gamma; double** dT_gamma2;
            double** dT_beta; double** dT_beta2;
    
            double** dT_gamma2_dev;
            double** dT_beta_dev; double** dT_beta2_dev; 

            dFx = (double**) malloc(size[2]*sizeof(double*));
            dFy = (double**) malloc(size[2]*sizeof(double*));
            drho = (double**) malloc(size[2]*sizeof(double*));
            dT_gamma  = (double**) malloc(size[2]*sizeof(double*));
            dT_gamma2 = (double**) malloc(size[2]*sizeof(double*));
        
            dFz = (double**) malloc(size[1]*sizeof(double*));
            dT_beta  = (double**) malloc(size[1]*sizeof(double*));
            dT_beta2 = (double**) malloc(size[1]*sizeof(double*));

            cudaMalloc((void**)& drho_dev, size[2]*sizeof(double*));
            cudaMalloc((void**)& dT_gamma2_dev, size[2]*sizeof(double*));
            cudaMalloc((void**)& dT_beta_dev, size[1]*sizeof(double*));
            cudaMalloc((void**)& dT_beta2_dev, size[1]*sizeof(double*));
            
            cudaMalloc((void**)& dFx[0],  size[0]*size[0]*sizeof(double));
            cudaMalloc((void**)& dFy[0],  size[1]*size[1]*sizeof(double));
            cudaMalloc((void**)& dFz[0], size[2]*size[2]*sizeof(double));

            cudaMalloc((void**)& d_potential[gpu_id],  size[0]*size[1]*size[2]*sizeof(double));
            cudaMemset( d_potential[gpu_id], 0,  size[0]*size[1]*size[2]*sizeof(double));

            cudaMalloc((void**)& d_density,  size[0]*size[1]*size[2]*sizeof(double));

            for(int i =0; i<size[2]; i++){
                cudaMalloc((void**)& drho[i], size[0]*size[1]*sizeof(double));
                cudaMalloc((void**)& dT_gamma[i],  size[0]*size[1]*sizeof(double));
                cudaMalloc((void**)& dT_gamma2[i], size[0]*size[1]*sizeof(double));
            }

            for(int j=0; j<size[1]; j++){
                cudaMalloc((void**)& dT_beta[j],  size[0]*size[2]*sizeof(double));
                cudaMalloc((void**)& dT_beta2[j],  size[0]*size[2]*sizeof(double));
            }
            gettimeofday(&end, NULL);
            spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
            std::cout << "Aloc (GPU): " << spent_time << " s  "<<gpu_id<<std::endl;
            gettimeofday(&start, NULL);
         
            cudaMemcpy( d_density, density, size[0]*size[1]*size[2]*sizeof(double), cudaMemcpyHostToDevice);
         
            cudaMemcpy(drho_dev, drho, size[2]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(dT_gamma2_dev, dT_gamma2, size[2]*sizeof(double*), cudaMemcpyHostToDevice);

            cudaMemcpy(dT_beta_dev, dT_beta, size[1]*sizeof(double*), cudaMemcpyHostToDevice);
            cudaMemcpy(dT_beta2_dev, dT_beta2, size[1]*sizeof(double*), cudaMemcpyHostToDevice);
         
            gettimeofday(&end, NULL);
            spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
            std::cout << "Copy (GPU): " << spent_time << " s  "<<gpu_id<<std::endl;
            gettimeofday(&start, NULL);
            /*
            for(int i =0; i< size[2]; i++){
                if(CUBLAS_STATUS_SUCCESS!=cublasSetMatrix(size[0], size[1], sizeof(double), rho[i], size[0], drho[i], size[0])){
                    std::cout << "shchoi!!!" <<std::endl;
                    exit(-1);
                }
            }
            */
            fold<<< num_blocks, num_threads >>> (d_density,drho_dev);
            gettimeofday(&end, NULL);
            spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
            std::cout << " Fold (GPU "<<gpu_id<<"): " << spent_time << " s  "<<gpu_id<<std::endl;


            while (true){
                gettimeofday(&start, NULL);
                if(gpu_count[gpu_id]%chunk_size==0){
                    omp_set_lock(&writelock);
                    gpu_i_t = i_t;
                    i_t+=chunk_size;
                    omp_unset_lock(&writelock);
                }
                else{
                    gpu_i_t++;
                }

                if(gpu_i_t>=t_size) break;

                std::cout  << gpu_i_t<< "   gpu"<<gpu_id <<std::endl;
                gpu_count[gpu_id]++;
                cublasSetMatrix(size[0], size[0], sizeof(double), F_xs[gpu_i_t], size[0], dFx[0], size[0] );
                cublasSetMatrix(size[1], size[1], sizeof(double), F_ys[gpu_i_t], size[1], dFy[0], size[1] );
                cublasSetMatrix(size[2], size[2], sizeof(double), F_zs[gpu_i_t], size[2], dFz[0], size[2] );
                for(int i_z=0; i_z<size[2]; i_z++){
                    cublasSetStream(handle, streamArray[i_z]);
                    if(CUBLAS_STATUS_SUCCESS!=cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, size[0], size[1], size[0], &one, (const double*) dFx[0], size[0], (const double *) drho[i_z], size[0], &zero, dT_gamma[i_z], size[0] )) std::cout << "shchoi" <<std::endl;
                }
         
                for(int i_z=0; i_z<size[2]; i_z++){
                    cublasSetStream(handle, streamArray[i_z]);
                    if(CUBLAS_STATUS_SUCCESS!=cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, size[0], size[1], size[1], &one, (const double*) dT_gamma[i_z], size[0], (const double*) dFy[0], size[1], &zero, dT_gamma2[i_z], size[0] )) std::cout << "shchoi9"<<std::endl;
                }
                transpose<<< num_blocks, num_threads >>> (dT_gamma2_dev, dT_beta_dev);
                for(int i_y=0; i_y<size[1]; i_y++){
                    cublasSetStream(handle, streamArray[i_y]);
                    if(CUBLAS_STATUS_SUCCESS!=cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, size[0], size[2], size[2], &w_t[gpu_i_t], (const double*) dT_beta[i_y], size[0], (const double*) dFz[0], size[2], &one, dT_beta2[i_y], size[0])) std::cout << "shchoi10" << std::endl;
                }
                for(int i_y=0; i_y<size[1]; i_y++){
                    cudaStreamSynchronize(streamArray[i_y]);
                }
                //cudaDeviceSynchronize();
                gettimeofday(&end, NULL);
                spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
                std::cout << gpu_i_t<<"  "<<  spent_time <<std::endl;
                if(gpu_i_t<t_size){
                    gpu_states[gpu_id]=0;
                }
                else{
                    gpu_states[gpu_id]=-1; // end tag
                    std::cout << "tag1  " << gpu_id <<std::endl;
                    break;
                }
                for(int i =0; i<gpu_id; i++){
                    if(gpu_states[i]==-1) gpu_state=-1;
                }
    	    }
            unfold <<< num_blocks, num_threads >>> ( (const double**) dT_beta2_dev, d_potential[gpu_id] );
            for(int i =0; i<size[2]; i++){
                cudaFree(drho[i]);
                cudaFree(dT_gamma[i]);
                cudaFree(dT_gamma2[i]);
            }
    
            for(int j=0; j<size[1]; j++){
                cudaFree(dT_beta[j]);
                cudaFree(dT_beta2[j]);
            }
    
            cudaFree(dT_gamma2_dev);
            cudaFree(dFx_dev);
            cudaFree(dFy_dev);
    
            cudaFree(drho_dev);
            cudaFree(dT_beta_dev);
            cudaFree(dT_beta2_dev);
            cudaFree(dFz_dev);
    
            cudaFree(dFx[0]);
            cudaFree(dFy[0]);
            cudaFree(dFz[0]);
            #pragma omp single nowait
            {
                gpu_sumvec <<< num_blocks, num_threads>>>( total_size, M_PI / (t_f*t_f), d_density, d_potential[gpu_id]);
            }
            gpu_potential[gpu_id] = (double*)mkl_malloc(total_size*sizeof(double),64);
            cublasGetVector(total_size,sizeof(double),d_potential[gpu_id],1,gpu_potential[gpu_id],1);
            
            for(int i=0; i<stream_size; i++){
                cudaStreamDestroy(streamArray[i]);
            }

            cudaFree(d_potential[gpu_id]);
            cudaFree(d_density);
    
            cudaFree(dFx[0]);
            cudaFree(dFy[0]);
            cudaFree(dFz[0]);
    
            free(dFx);
            free(dFy);
            free(drho);
            free(dT_gamma);
            free(dT_gamma2);
    
            free(dFz);
            free(dT_beta);
            free(dT_beta2);
        }
    }
    std::cout << "end parallel region"<< std::endl;
    for(int gpu_id=1; gpu_id<N_GPU; gpu_id++){
        #pragma omp parallel for num_threads(N_THREADS)
        for(int i=0; i<total_size; i++){
            gpu_potential[0][i]+=gpu_potential[gpu_id][i];
        }
    }
/*
    for(int i_group = 0; i_group < n_groups; i_group++){
        sumvec(total_size,  Ts[i_group], potential);
    }
*/
    //////////////////// COPY DATA FROM GPU TO CPU ///////////////
    /////////////////////// CPU MEMORY DEALLOC //////////////////////////
    for(int k=0; k<size[2]; k++){
        mkl_free( rho[k] );
    }
    mkl_free(rho);

    cudaDeviceSynchronize();
    sumvec(total_size, gpu_potential[0], T);
    memcpy(potential,T,total_size*sizeof(double));
    mkl_free( T);
    /////////////////// FREE GPU MEMORY //////////////////////////
    //delete[] streamArray;
/*
    for(int i =0; i< size[2]; i++){
        cudaStreamDestroy(streams[i]);
    }
*/
    /////////////////////FREE CPU MEMORY //////////////////////////
    for(int gpu_id=0;gpu_id<N_GPU;gpu_id++){
        mkl_free( gpu_potential[gpu_id]);
        std::cout << "gpu_count (" << gpu_id<< ") \t" << gpu_count[gpu_id] <<std::endl;
    }
    delete[] gpu_potential;
    omp_destroy_lock(&writelock);
    std::cout << "cpu_count\t" << cpu_count <<std::endl;
    return 0;
}



int gen_grid(int* size, double* scaling, double**& grid){

    for (int i =0;i<3;i++){
        for (int j =0;j<size[i];j++){
            grid[i][j]=-scaling[i]*(0.5*(size[i]-1)-j);
        }
    }
    return 0;
}
// Given the lower and upper limits of integration x1 and x2, and given n, this routine returns
// arrays x[1..n] and w[1..n] of length n, containing the abscissas and weights of the Gauss-
// Legendre n-point quadrature formula.
void gauleg(const double x1, const double x2, double* x, double* w, const int n){
    double EPS = 3.0E-11; // Relative precision
    int m, j, i;
    double z1, z, xm, xl, pp, p3, p2, p1; // High precision is a good idea for this routine.
    m = (n+1)/2;      // The roots are symmetric in the interval, so
    xm = 0.5*(x2+x1); // we only have to find half of them.
    xl = 0.5*(x2-x1);
    for(i=1;i<=m;i++){ // Loop over the desired roots.
        z = cos(3.141592654*(i-0.25)/(n+0.5));
        // Starting with the above approximation to the ith root, we enter the main loop of
        // refinement by Newton’s method.
        do{
            p1 = 1.0;
            p2 = 0.0;
            for(j=1;j<=n;j++){ // Loop up the recurrence relation to get the
                p3 = p2;       // Legendre polynomial evaluated at z.
                p2 = p1;
                p1 = ((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j;
            }
            // p1 is now the desired Legendre polynomial. We next compute pp, its derivative,
            // by a standard relation involving also p2, the polynomial of one lower order.
            pp = n*(z*p1-p2)/(z*z-1.0);
            z1 = z;
            z = z1-p1/pp; // Newton’s method.
        } while (fabs(z-z1) > EPS);
        x[i-1] = xm-xl*z;                  // Scale the root to the desired interval,
        x[n-i] = xm+xl*z;              // and put in its symmetric counterpart.
        w[i-1] = 2.0*xl/((1.0-z*z)*pp*pp); // Compute the weight
        w[n-i] = w[i-1];                 // and its symmetric counterpart.
    }
    return;
}

int t_sampling(const int num_points1, const int num_points2,
        const double t_i, const double t_l, const double t_f,
        double*& t_values, double*& w_t)
{
    // New method by Sundholm (JCP 132, 024102, 2010).
    // Within two-divided regions ([t_i,t_l], [t_l,t_f]),
    // num_points1, num_points2 quadrature points are made, respectively.

    // Linear coord region:  [t_i, t_l]

    double* x_leg = new double [num_points1];
    double* w_leg = new double [num_points1];
    gauleg(t_i, t_l, x_leg, w_leg, num_points1);

    for(int j=0; j<num_points1; j++){
        t_values[j] = x_leg[j];
        w_t[j] = w_leg[j]*2.0/sqrt(M_PI);
    }

    delete[] w_leg;
    delete[] x_leg;

    // Logarithmic coord region: [t_l, t_f]
    x_leg = new double [num_points2];
    w_leg = new double [num_points2];
    gauleg(log(t_l), log(t_f), x_leg, w_leg, num_points2);

    // Return the log-coord-partitioned points back to linear t-space.
    double s_p, w_p;
    for(int j=0; j<num_points2; j++){
        s_p = x_leg[j];
        w_p = w_leg[j];
        x_leg[j] = exp(s_p);
        w_leg[j] = w_p * exp(s_p);
    }

    for(int j=0; j<num_points2; j++){
        t_values[num_points1+j] = x_leg[j];
        w_t[num_points1+j] = w_leg[j]*2.0/sqrt(M_PI);
    }

    delete[] w_leg;
    delete[] x_leg;

    return 0;
}

int init_density(double* position, const double coef, const double exponent,
        int* size, double* scaling, double** grid, double*& density, double*& potential)
{
    double x,y,z; 
    double r2;
    const double exponent2 = exponent*exponent;
    const double weight = scaling[0]*scaling[1]*scaling[2];
    for (int i_x=0; i_x <size[0]; i_x++){
        for (int i_y=0; i_y<size[1]; i_y++){
            for(int i_z=0; i_z<size[2]; i_z++){
                x = grid[0][i_x]-position[0];
                y = grid[1][i_y]-position[1];
                z = grid[2][i_z]-position[2];
                r2 = x*x+y*y+z*z;
                density[i_x*size[1]*size[2]+i_y*size[2]+i_z] += coef*pow(exponent2/M_PI,1.5)
                    *exp(-1*exponent2*r2)*sqrt(weight);
                potential[i_x*size[1]*size[2]+i_y*size[2]+i_z] += coef*erf(exponent*sqrt(r2))/sqrt(r2);
            }
        }
    }
    return 0;
}

int read_input(std::string filename, int*& size, double*& scaling,
                int& num_gaussian, double**& positions, double*& coef, double*& exponents,
                int& num_points1, int& num_points2, double& t_i, double& t_l, double& t_f)
{

    std::ifstream file(filename.c_str());
    file>>size[0]; file>>size[1]; file>>size[2];
    file>>scaling[0]; file>>scaling[1]; file>>scaling[2];
    file>>num_points1; file>>num_points2;
    file>>t_i; file>>t_l; file>>t_f;
    file>>num_gaussian;

/*
    FILE *file;
    file = fopen( filename.c_str(), "r" );
    if( file == NULL )
    {
        perror( "fopen" );
        return -1;
    }

    fscanf( file, "%d %d %d \n", &size[0], &size[1], &size[2] );
    fscanf( file, "%lf %lf %lf\n", &scaling[0], &scaling[1], &scaling[2] );
    fscanf( file, "%d %d \n", &num_points1, &num_points2 );
    fscanf( file, "%lf %lf %lf\n", &t_i, &t_l, &t_f);
    fscanf( file, "%d \n", &num_gaussian);
*/
    positions = new double*[num_gaussian];
    coef = new double[num_gaussian];
    exponents = new double[num_gaussian];

    //int linenum = 4;
    //int count_gaussian = 0;
    for (int i =0; i<num_gaussian; i++){
        positions[i] = new double[3];
        file>>positions[i][0]; file>>positions[i][1]; file>>positions[i][2];
        file>>coef[i]; file>>exponents[i];
    }
/*
    while( count_gaussian<num_gaussian)
    {
        linenum++;

        positions[count_gaussian] = new double[3];
        if( fscanf( file, "%lf %lf %lf \n", &positions[count_gaussian][0], &positions[count_gaussian][1], &positions[count_gaussian][2] ) != 3 ) {
            fprintf( stderr, "Error in file on line %i\n", linenum );
        }
        linenum++;
        if( fscanf( file, "%lf %lf \n", &coef[count_gaussian], &exponents[count_gaussian])) {
            fprintf( stderr, "Error in file on line %i\n", linenum );
        }
        count_gaussian++;
    }
*/
    return 0;
}

double compute_energy(const int total_size, double* scaling, double* potential, double* density){
    double return_val=0.0;
    for(int i =0; i < total_size; i++){
        return_val += density[i]*potential[i];
    }
    return 0.5*return_val*sqrt(scaling[0]*scaling[1]*scaling[2]);
}

int main(int argc, char* argv[]){

    if(argc != 2){
        std::cout<< "Wrong input argument. \n";                                                                           
        std::cout<< "You have to write 'executable inputfile' \n" ;
        std::cout<< "ex) ./exe input.txt \n" ;
        exit(EXIT_FAILURE);
    }
    std::string filename=argv[1];


    double* scaling = new double[3];    
    int* size = new int[3];
    int num_points1; int num_points2;
    double t_i; double t_l; double t_f;

    int num_gaussian;
    double** positions;
    double* coef; double* exponents;
    read_input(filename, size, scaling, num_gaussian, positions, coef, exponents, num_points1, num_points2, t_i, t_l, t_f);

    time_t st;
    st = std::clock();

    cudaSetDevice (1);
    cudaDeviceProp deviceProperties;
    cudaGetDeviceProperties(&deviceProperties, 0);
    std::cout << "set device : " << double(std::clock()- st)/CLOCKS_PER_SEC << "s"<<std::endl;
    std::cout << deviceProperties.name <<std::endl;

    const int t_size = num_points1+num_points2;
    const int total_size = size[0]*size[1]*size[2];
    std::cout << "======== input information ========" <<std::endl;
    std::cout << "size: " << size[0] <<"," << size[1]<<"," <<size[2] << std::endl;
    std::cout << "total_size: " <<  total_size <<std::endl;
    std::cout << "scaling: " <<scaling[0] << "," << scaling[1] <<"," << scaling[2] <<std::endl;
    std::cout << "num_points1: " << num_points1 << std::endl << "num_points2: " << num_points2 << std::endl;
    std::cout << "num_gaussian: " << num_gaussian << std::endl;
    std::cout << "===================================" <<std::endl;
    double* t_values = new double[t_size];
    double* w_t = new double[t_size];


    t_sampling(num_points1, num_points2, t_i, t_l, t_f, t_values, w_t );

    double** grid = new double*[3];
    grid[0] = new double[size[0]];
    grid[1] = new double[size[1]];
    grid[2] = new double[size[2]];

    gen_grid(size,scaling,grid);

    //std::cout << "=====grid======="<<std::endl;
    //for(int i=0;i<3;i++){
    //    for(int j=0; j< size[i]; j++){
    //        std::cout << grid[i][j] << "\t";
    //    }
    //    std::cout << "\n";
    //}
    //std::cout << "================"<<std::endl;

    double** F_xs = new double*[t_size];
    double** F_ys = new double*[t_size];
    double** F_zs = new double*[t_size];

    double* density = new double[total_size]();
    double* potential = new double[total_size]();
    double* anal_potential = new double[total_size]();

    
    st = std::clock();
    for (int i=0; i<num_gaussian; i++){
        init_density(positions[i], coef[i], exponents[i], size, scaling, grid, density,anal_potential);
    }
    std::cout << "initialize density: " << double(clock()- st)/CLOCKS_PER_SEC << "s"<<std::endl;  
    double density_norm=0.0;
    double anal_energy=0.0;
    //#pragma omp parallel for reduction(+ : density_norm)
    for(int j=0; j<total_size; j++){
        density_norm+=density[j];
        anal_energy+=0.5*anal_potential[j]*density[j];
    }
    density_norm*=sqrt(scaling[0]*scaling[1]*scaling[2]);
    anal_energy*=sqrt(scaling[0]*scaling[1]*scaling[2]);
    std::cout << "norm of density:  " << density_norm <<std::endl;
    st = std::clock();
    construct_F(0, t_size, t_values, size, scaling, grid, F_xs);
    construct_F(1, t_size, t_values, size, scaling, grid, F_ys);
    construct_F(2, t_size, t_values, size, scaling, grid, F_zs);

    std::cout << "construct F matrices: " << double(clock()- st)/CLOCKS_PER_SEC << "s"<<std::endl;

    struct timeval start, end;
    gettimeofday(&start, NULL);

    
    compute_potential( size, t_size, t_values, w_t, density, F_xs, F_ys, F_zs, potential, t_f);

    gettimeofday(&end, NULL);
    double spent_time =  ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
    std::cout << "! compute_potential: " << spent_time << "s"<<std::endl;


    double energy = compute_energy(total_size, scaling, density, potential);
    std::cout << "energy:  "  <<std::setprecision(15) <<energy << "/   analytic:  "<< anal_energy <<std::endl;

    delete[] grid[0];
    delete[] grid[1];
    delete[] grid[2];
    delete[] grid;
    delete[] density;
    delete[] potential;
    for(int i_t=0;i_t<t_size;i_t++){
        //delete[] F_xs[i_t];
        //delete[] F_ys[i_t];
        //delete[] F_zs[i_t];
        mkl_free( F_xs[i_t] );
        mkl_free( F_ys[i_t] );
        mkl_free( F_zs[i_t] );
    }
    delete[] F_xs;
    delete[] F_ys;
    delete[] F_zs;

    return 0;
}
